//
//  CollectionViewCell.swift
//  Basic Collection View
//
//  Created by Steven Hertz on 12/17/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var checkImage: UIImageView! {
        didSet {
            checkImage.alpha = 0.0
            let tintableImage = checkImage.image?.withRenderingMode(.alwaysTemplate)
            print("set it tintable")
             checkImage.image = tintableImage
            checkImage.tintColor = UIColor.yellow

           //  checkImage.image = UIImage(systemName: <#T##String#>, withConfiguration: UIImage.Configuration?)  //UIImage(imageLiteralResourceName: "myImageName").withRenderingMode(.alwaysTemplate)
            // checkImage.tintColor = UIColor.red
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let redView = UIView(frame: bounds)
        
        redView.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        backgroundView = redView
        
        let blueView = UIView(frame: bounds)
        blueView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        selectedBackgroundView = blueView
        
    }
    
    func showIcon() {
        checkImage.alpha = 1.0
    }
    
    func hideIcon() {
        checkImage.alpha = 0.0
    }

    
}
