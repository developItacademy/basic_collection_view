//
//  ViewController.swift
//  Basic Collection View
//
//  Created by Steven Hertz on 12/17/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    enum SelectionMode {
        
        case multipleEnabled
        case multipleDisabled
        
       
        var allTitles: (buttonTitle: String, barTitle: String) {
            switch self {
            case .multipleDisabled:
                return ("Select", "Hello")
            case .multipleEnabled:
                return ("Cancel", "Select Items")
            }
        }

        
        var allowsMultipleSelection: Bool {
            switch self {
            case .multipleDisabled:
                return false
            case .multipleEnabled:
                return true
            }
        }

        mutating func toggle()  {
            switch self {
            case .multipleEnabled:
                self = .multipleDisabled
            case .multipleDisabled:
                self = .multipleEnabled
            }
        }
        
    }
    
    enum ItemsToDisplay {
         
         case students
         case devices
         
        
         var allTitles: (buttonTitle: String, barTitle: String) {
             switch self {
             case .devices:
                 return ("Select", "Hello")
             case .students:
                 return ("Cancel", "Select Items")
             }
         }

         
         var allowsMultipleSelection: Bool {
             switch self {
             case .devices:
                 return false
             case .students:
                 return true
             }
         }

         mutating func toggle()  {
             switch self {
             case .students:
                 self = .devices
             case .devices:
                 self = .students
             }
         }
         
     }
     
    
    
    var tabBarHeight : CGFloat = 0.0
    
    var saveRow = 0
    var rowSelected : Int = 0
    
    var itemsToDisplay: ItemsToDisplay = .students
    
    var selectionMode : SelectionMode = .multipleDisabled {
        
        willSet {
            switch newValue {
            case .multipleDisabled:
                collectionVW.indexPathsForSelectedItems?.forEach{
                    if let cell = collectionVW.cellForItem(at: $0) as? CollectionViewCell {
                        cell.hideIcon()
                    }
                    collectionVW.deselectItem(at: $0, animated: false)
                }
//                if (!(tabBarController?.tabBar.frame.size.height.isEqual(to: 0))!) {
//                    tabBarHeight = (self.tabBarController?.tabBar.frame.size.height)!
//                 }
//                 tabBarController?.tabBar.frame.size.height   = tabBarHeight
                self.tabBarController?.tabBar.isHidden = false
                //self.tabBarController?.tabBar.layer.zPosition = 0
                navigationController?.setToolbarHidden(true, animated: true)
            case .multipleEnabled:
                addBarButton.isEnabled = false
//                tabBarHeight = (tabBarController?.tabBar.frame.size.height)!
//                tabBarController?.tabBar.frame.size.height   = 0
                self.tabBarController?.tabBar.isHidden = true
                //self.tabBarController?.tabBar.layer.zPosition = -1
                navigationController?.setToolbarHidden(false, animated: false)
                
                print("multiple enablled")
            }
        }
        
        didSet {
            collectionVW.allowsMultipleSelection = selectionMode.allowsMultipleSelection
            barButtonSelectCancel.title = selectionMode.allTitles.buttonTitle
            navigationItem.title = selectionMode.allTitles.barTitle
        }
    }
    
    var addBarButton: UIBarButtonItem = UIBarButtonItem()
    
    @IBOutlet weak var barButtonSelectCancel: UIBarButtonItem!
    
    @IBOutlet weak var collectionVW: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         print("in view will appear row", saveRow)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in view did load  row  ", saveRow)
        
        
        addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        addBarButton.isEnabled = false
        toolbarItems = [addBarButton, spacer]
        navigationController?.setToolbarHidden(true, animated: false)
        
        
        barButtonSelectCancel.title = "Select"
        collectionVW.delegate = self
        collectionVW.dataSource = self
        collectionVW.allowsMultipleSelection = false
        
        
        self.tabBarController?.tabBar.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonTouched(_ sender: Any) {
        collectionVW.indexPathsForSelectedItems?.forEach{ print($0.row) }
    }
    
    @IBAction func buttonClicked(_ sender: UIBarButtonItem) {
        selectionMode.toggle()
        print(selectionMode)
        // sender.title = selectionMode.buttonTitle
        print(sender.title)
    }
    @objc func addTapped() {
        print("button tapped")
        performSegue(withIdentifier: "gotoDetail", sender: nil)
    }
    
    func setBarButtonSelected() {
        barButtonSelectCancel.title = "Select"
        selectionMode = .multipleDisabled
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("jjjjj")
        return 150
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        switch itemsToDisplay {
        case .students:
            // redView.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cell.backgroundView?.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        case .devices:
            cell.backgroundView?.backgroundColor = #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1)
        }
        print(indexPath.row)
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("item selected")
//    }
    
    
}


extension ViewController {
    
    /// - Tag: highlight
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1)
        }
        print("itemhightlighted")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = nil
        }
        print("itemUNhightlighted")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch selectionMode {
        case .multipleDisabled:
            rowSelected = indexPath.row
            saveRow = indexPath.row
            collectionView.deselectItem(at: indexPath, animated: false)
            performSegue(withIdentifier: "gotoDetail", sender: nil)
            
        case .multipleEnabled:
            print("itemselected")
            if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
                cell.showIcon()
            }
            addBarButton.isEnabled = true
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("itemDeselected")
        if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            cell.hideIcon()
        }
        guard let count = collectionView.indexPathsForSelectedItems?.count else {return}
        if count < 1 {
            addBarButton.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch selectionMode {
            
        case .multipleDisabled:
            switch segue.identifier {
            case "gotoDetail":
                
                // self.hidesBottomBarWhenPushed = true
                guard let studentProfileStaticTableVC = segue.destination as? DetailViewController else { fatalError(" could not segue ") }
                studentProfileStaticTableVC.msg = "We clicked on the \(rowSelected) in other screen"
                print("finished Segue")
            default:
                break
            }

        case .multipleEnabled:
            // self.hidesBottomBarWhenPushed = true
            guard let studentProfileStaticTableVC = segue.destination as? DetailViewController else { fatalError(" could not segue ") }
            guard let items = collectionVW.indexPathsForSelectedItems?.compactMap({ (ip)  in
                String(ip.row)
            }) else { return }
            let multiSelection  = items.joined(separator: ", ")
            studentProfileStaticTableVC.msg = "We had multiple rows clicked here they are \(multiSelection) "
            selectionMode.toggle()

            print("finished Segue")
        }
        
    }
    
}


 
     
