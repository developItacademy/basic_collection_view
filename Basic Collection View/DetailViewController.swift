//
//  DetailViewController.swift
//  Basic Collection View
//
//  Created by Steven Hertz on 12/17/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var msg = ""
    
    
    @IBOutlet weak var msgFromPrev: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        msgFromPrev.text = msg

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
